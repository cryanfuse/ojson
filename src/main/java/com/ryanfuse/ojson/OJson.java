/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.ryanfuse.ojson.io.OJsonIOUtils;
import com.ryanfuse.ojson.mapping.JsonToOJsonMapper;
import com.ryanfuse.ojson.mapping.OJsonToJsonMapper;
import com.ryanfuse.ojson.mapping.StringMapper;

/*
 * Conversion between plain JSON and Optimised JSON (OJSON).
 *
 * Any valid JSON value may be supplied; simple types, null, array or object.  
 */
public class OJson {

    public static String toJSON(File ojsonFile) {
    	if (ojsonFile == null) {
    		return null;
    	}
    	try {
			return toJSON(OJsonIOUtils.readFileToString(ojsonFile));
		} catch (IOException e) {
			System.err.println("Error reading ojson file: " + e.getMessage());
		}
		return null;
    }

    public static String toJSON(Reader ojsonReader) throws IOException {
        Reader bufferedOJsonReader = ojsonReader.markSupported()
            ? ojsonReader
            : new BufferedReader(ojsonReader);
        return toJSON(OJsonIOUtils.toString(bufferedOJsonReader));
    }
    
    public static String toJSON(InputStream ojsonStream) throws IOException {
    	return toJSON(ojsonStream, OJsonOptions.defaultOptions());
    }

    public static String toJSON(InputStream ojsonStream, OJsonOptions options) throws IOException {
    	if (ojsonStream == null) {
    		return null;
    	}
    	return toJSON(readFromStream(ojsonStream, options.getEncoding()));
    }

    /*
     * Convert OJSON -> standard JSON, with options
     */
    public static String toJSON(String ojson) {
    	return toJSON(ojson, OJsonOptions.defaultOptions());
    }

    /*
     *  Convert standard JSON string -> OJSON.
     *  
     *  This conforms to the strict JSON specification which states that a JSON text does
     *  not need to begin with an object, instead can be any valid JSON value type.
     */
    public static String toJSON(String json, OJsonOptions options) {
    	// number, boolean or null
    	Object val = JSONObject.stringToValue(json);
    	String str = simpleValue(val, json);
    	if (str != null) {
    		return json; // simple type (boolean, number, null) 
    	}
    	// test for string
    	JSONTokener tok = new JSONTokener(json);
    	Object rootValue = tok.nextValue();
    	if (rootValue instanceof JSONArray) {
    		JSONArray arr = (JSONArray)rootValue;
    		return toJSON(arr, options);
    	} else if (rootValue instanceof String) {
    		return json; // string type
    	}
    	// Object structure
        return toJSON(new JSONObject(json), options);
    }

    public static String toJSON(JSONObject json) {
        JsonToOJsonMapper mapper = new JsonToOJsonMapper(json, OJsonOptions.defaultOptions());
        return mapper.mapToString();
    }
    
    public static String toJSON(JSONObject json, OJsonOptions options) {
        OJsonToJsonMapper mapper = new OJsonToJsonMapper(json, options);
        return mapper.mapToString();
    }

    public static String toJSON(JSONArray json) {
        return toJSON(json, OJsonOptions.defaultOptions());
    }

    public static String toJSON(JSONArray json, OJsonOptions options) {
        OJsonToJsonMapper mapper = new OJsonToJsonMapper(json, options);
        return mapper.mapToString();
    }

    /*
     *  Convert standard JSON string -> OJSON.
     *  
     *  This conforms to the strict JSON specification which states that a JSON text does
     *  not need to begin with an object, instead can be any valid JSON value type.
     */
    public static String toOJSON(File jsonFile) {
    	if (jsonFile == null) {
    		return null;
    	}
    	try {
			return toOJSON(OJsonIOUtils.readFileToString(jsonFile));
		} catch (IOException e) {
			System.err.println("Error reading json file: " + e.getMessage());
		}
		return null;
    }

    public static String toOJSON(Reader jsonReader) throws IOException {
        Reader bufferedJsonReader = jsonReader.markSupported()
            ? jsonReader
            : new BufferedReader(jsonReader);
        //IOUtils.toString(bufferedJsonReader);
        return toOJSON(OJsonIOUtils.toString(bufferedJsonReader));
        
    }
    
    public static String toOJSON(InputStream jsonStream) throws IOException {
    	return toOJSON(jsonStream, OJsonOptions.defaultOptions());
    }

    public static String toOJSON(InputStream jsonStream, OJsonOptions options) throws IOException {
    	if (jsonStream == null) {
    		return null;
    	}
    	return toOJSON(readFromStream(jsonStream, options.getEncoding()));
    }

    public static String toOJSON(String json) {
    	return toOJSON(json, OJsonOptions.defaultOptions());
    }

    public static String toOJSON(String json, OJsonOptions options) {
    	// number, boolean or null
    	Object val = JSONObject.stringToValue(json);
    	String str = simpleValue(val, json);
    	if (str != null) {
    		return json; // simple type (boolean, number, null) 
    	}
    	// test for string
    	JSONTokener tok = new JSONTokener(json);
    	Object rootValue = tok.nextValue();
    	if (rootValue instanceof JSONArray) {
    		JSONArray arr = (JSONArray)rootValue;
    		return toOJSON(arr, options);
    	} else if (rootValue instanceof String) {
    		StringMapper sm = new StringMapper(options);
    		if (sm.matchesOjsonTag(json)) {
    			return sm.escapeString(json);
    		}
    		return json; // string type
    	}
    	// Object structure
        return toOJSON(new JSONObject(json), options);
    }

	/* 
     *  Convert standard JSON object -> OJSON string 
     */
    public static String toOJSON(JSONObject json) {
        return toOJSON(json, OJsonOptions.defaultOptions());
    }

    public static String toOJSON(JSONObject json, OJsonOptions options) {
        JsonToOJsonMapper mapper = new JsonToOJsonMapper(json, options);
        return mapper.mapToString();
    }

    public static String toOJSON(JSONArray json) {
        return toOJSON(json, OJsonOptions.defaultOptions());
    }

    public static String toOJSON(JSONArray json, OJsonOptions options) {
        JsonToOJsonMapper mapper = new JsonToOJsonMapper(json, options);
        return mapper.mapToString();
    }

    // Convenience method to pretty print JSON text
    public static String formattedText(String json, int tab) {
    	return prettyText(json, tab);
    }

    // Convenience method to compress JSON text, removing whitespace
    public static String compressedText(String json) {
    	return prettyText(json, 0);
    }
    
    private static String prettyText(String json, int tab) {
    	// number, boolean or null
    	Object val = JSONObject.stringToValue(json);
    	String str = simpleValue(val, json);
    	if (str != null) {
    		return json; // simple type (boolean, number, null) 
    	}
    	// test for string
    	JSONTokener tok = new JSONTokener(json);
    	Object rootValue = tok.nextValue();
    	if (rootValue instanceof JSONArray) {
    		JSONArray arr = (JSONArray)rootValue;
    		return tab > 0 ? arr.toString(tab) : arr.toString();
    	} else if (rootValue instanceof String) {
    		return json;
    	}
    	// Object structure
    	JSONObject jsonObject = new JSONObject(json);
		return tab > 0 ? jsonObject.toString(tab) : jsonObject.toString();
    }

    private static String readFromStream(InputStream inputStream, String encoding) throws IOException {
    	ByteArrayOutputStream result = new ByteArrayOutputStream();
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = inputStream.read(buffer)) != -1) {
    		result.write(buffer, 0, length);
    	}
    	return result.toString(encoding);
    }
    
    private static String simpleValue(Object val, String json) {
    	if (val == JSONObject.NULL) {
    		return json;
    	}
		if (val instanceof Long || val instanceof Integer || val instanceof Double || val instanceof Boolean) {
			return json;
		}
		// string or array or object
		return null;
	}
}
