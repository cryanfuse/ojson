/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import java.io.File;
import java.io.IOException;

import com.ryanfuse.ojson.io.OJsonIOUtils;

/*
 * Example client to read in JSON text and map to OJSON. The result is saved to a file or displayed 
 * to standard output.
 * 
 * If -u (unmap) option is specified, the mapping is reversed, from OJSON to JSON.
 * 
 * Usage: Client [-u] <path/to/input-file> [<output-file>]
 */
public class Client {

	public static void main(String[] args) {
		String fromFile = null, toFile = null;
		boolean unmap = false;
		boolean pretty = true;
		switch (args.length) {
		case 0: 
			usage(); 
			break;
		case 1: 
			fromFile = args[0];
			break;
		case 2:
			if (args[0].equals("-u")) {
				unmap = true;
				fromFile = args[1];
			} else {
				fromFile = args[0];
				toFile = args[1];
			}
			break;
		case 3: 
			if (!args[0].equals("-u")) {
				usage();
			}
			unmap = true;
			fromFile = args[1];
			toFile = args[2];
			break;
		default:
			usage();
		}
		if (args.length < 1) {
			usage();
		}
		Client client = new Client();
		try {
			if (unmap) {
				client.mapOJsonToJson(fromFile, toFile);
			} else {
				client.mapJsonToOJson(fromFile, toFile);
			}
		} catch (IOException e) {
			error(e.getMessage());
		}
	}
	
	private void mapJsonToOJson(String fromFile, String toFile) throws IOException {
		File json = new File(fromFile);
		String jsonText = OJsonIOUtils.readFileToString(json);
		// Map to optimised JSON
		String ojsonText = OJson.toOJSON(jsonText);
		if (ojsonText == null) {
			error("Json file " + fromFile + " mapped to empty OJson text");
		}
		// Display the result
		String prettyOJson = OJson.formattedText(ojsonText, 4);
		if (toFile != null) {
			saveFile(prettyOJson, toFile, "JSON", "OJSON");
		} else {
			displayText(prettyOJson, "JSON", "OJSON");
		}
		String compressedJson = OJson.compressedText(jsonText);
		reportStats(compressedJson.length(), ojsonText.length(), true);
	}

	private void mapOJsonToJson(String fromFile, String toFile) throws IOException {
		File ojsonFile = new File(fromFile);
		// Map to plain JSON
		String ojsonText = OJsonIOUtils.readFileToString(ojsonFile);
		String jsonText = OJson.toJSON(ojsonText);
		if (jsonText == null) {
			error("OJson file " + fromFile + " mapped to empty Json text");
		}
		// Display the result
		String prettyOJson = OJson.formattedText(jsonText, 4);
		if (toFile != null) {
			saveFile(prettyOJson, toFile, "OJSON", "JSON");
		} else {
			displayText(prettyOJson, "OJSON", "JSON");
		}
		String compressedOJson = OJson.compressedText(ojsonText);
		reportStats(compressedOJson.length(), jsonText.length(), false);
	}

	private void saveFile(String jsonOrOJsonText, String jsonOrOJsonFilename, String fromType, String toType) {
		File saveFile = new File(jsonOrOJsonFilename);
		if (saveFile.exists()) {
			error("Save " + fromType + " to file " + jsonOrOJsonFilename + " failed. File already exists. Remove or rename this file and re-run.");
		}
		try {
			OJsonIOUtils.write(saveFile, jsonOrOJsonText);
		} catch (IOException e) {
			error("Unable to save " + fromType + " to file " + jsonOrOJsonFilename + ". Error: " + e.getMessage());
		}
		System.out.println(fromType + " has been mapped to " + toType + " and saved in the file " + jsonOrOJsonFilename);
	}

	private void reportStats(int fromLen, int toLen, boolean expectReduction) {
		boolean reduced = fromLen > toLen;
		float diff = reduced ? (100 - (100 * toLen) / fromLen) 
				: (100 - (100 * fromLen) / toLen);
		String percentage = String.format("%.1f", diff) + "%";
		StringBuilder sb = new StringBuilder("\n");
		if (expectReduction) {
			if (reduced) {
				sb.append("Expected reduction in size of ").append(percentage);
				sb.append(" from [" + fromLen + "] to [" + toLen + "] characters.\n"); 
			} else {
				sb.append("Unexpected increase in size of ").append(percentage);
				sb.append(" from [" + fromLen + "] to [" + toLen + "] characters.\n");
				sb.append(" input data not suitable for OJson mapping.");
			}
		} else {
			// should increase
			if (reduced) {
				sb.append("Unexpected decrease in size of ").append(percentage);
				sb.append(" from [" + fromLen + "] to [" + toLen + "] characters.\n");
				sb.append(" input data not suitable for OJson mapping.");
			} else {
				sb.append("Expected increase in size of ").append(percentage);
				sb.append(" from [" + fromLen + "] to [" + toLen + "] characters.\n"); 
			}
		}
		System.out.println(sb.toString());
	}


	private void displayText(String jsonOrOJson, String from, String to) {
		System.out.println(from + " has been mapped to the following " + to);
		System.out.println("--------------------------------------------\n");
		System.out.println(jsonOrOJson);
	}

	private static void error(String msg) {
		System.out.println("Error: " + msg);
		System.exit(-1);
	}
	private static void usage() {
		System.out.println("Usage: Client [-u] <input-file> [ <output-file>]");
		System.out.println(" where -u maps OJSON back to JSON (default is to map JSON to OJSON).");
		System.exit(-1);
	}
}
