/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

/**
 * Options controlling the mapping from JSON to Optimised JSON (OJSON).
 * 
 * @author Craig Ryan
 */
public class OJsonOptions {
  
    public static final int DEFAULT_MIN_STRING_LENGTH = 5;
	public static final int DEFAULT_MIN_ARRAY_ELEMENTS = 3;
	public static final int DEFAULT_MIN_MATCHING_OBJECT_KEYS = 3;
	public static final boolean DEFAULT_STRING_REF_ENABLED = true;
	public static final boolean DEFAULT_STRING_REF_CLEANING_ENABLED = false;
	public static final boolean DEFAULT_TEMPLATING_ENABLED = true;
	private static final String DEFAULT_ENCODING = "UTF-8";

	private boolean stringReferencesEnabled = DEFAULT_STRING_REF_ENABLED;
    private boolean stringReferenceCleaningEnabled = DEFAULT_STRING_REF_CLEANING_ENABLED;
    private boolean templatingEnabled = DEFAULT_TEMPLATING_ENABLED;
    private int minimumArrayElements = DEFAULT_MIN_ARRAY_ELEMENTS;
    private int minimumStringLength = DEFAULT_MIN_STRING_LENGTH; 
    private int minimumMatchingObjectKeys = DEFAULT_MIN_MATCHING_OBJECT_KEYS;

    private String encoding = DEFAULT_ENCODING;
    
    public static OJsonOptions defaultOptions() {
        return new OJsonOptions();
    }

    public OJsonOptions() {
    }
    
    /*
     * Is string referencing enabled to eliminate string duplicates?
     * 
     * If enabled, this processes the JSON text a second time to remove 
     * all references not in use. For example, "@10:hello" in JSON text 
     * which contains no other occurrences of "hello" is not of use and
     * can be removed allowing the first occurrence to remain as "hello".
     */
    public boolean isStringReferencesEnabled() {
        return stringReferencesEnabled;
    }

    /*
     * Is templating of arrays enabled?
     */
    public boolean isTemplatingEnabled() {
        return templatingEnabled;
    }

    /*
     * Is string reference cleaning enabled to remove un-used string references?
     *
     * For JSON text containing a lot of unique strings, this will remove more
     * string references tags not in use and reduce payload size. 
     */
    public boolean isStringReferenceCleaningEnabled() {
        return stringReferenceCleaningEnabled;
    }

    /*
     * The minimum number of array elements before applying templating.
     * 
     * For only a few objects in an array, this avoids non-beneficial 
     * templating of only a few objects across many arrays.  
     */
	public int getMinimumArrayElements() {
		return minimumArrayElements;
	}

	/*
	 * Minimum number of common keys across objects in an array.
	 * 
	 * This can be tuned high in order to focus on those objects
	 * with the most number of common (and present) keys. Setting
	 * a lower value allows objects with many optional or mismatching
	 * keys within an array to be templated.  
	 */
	public int getMinimumMatchingObjectKeys() {
		return minimumMatchingObjectKeys;
	}

    /*
     * Minimum string length before adding optimising string references.
     * 
     * By setting this value high then JSON text with many small unique strings
     * will not have references applied. A low value should not go below a logical
     * limit. For example, large text may generate multi-digit references, such as
     * '@45:string" which comprises 4 characters. If the string the reference 
     * optimises is 4 or less characters then references will not reduce the amount
     * of text.
     */
	public int getMinimumStringLength() {
		return minimumStringLength;
	}

	public void setMinimumArrayElements(int minimumArrayElements) {
		this.minimumArrayElements = minimumArrayElements;
	}

    public void setStringReferencesEnabled(boolean stringReferencesEnabled) {
        this.stringReferencesEnabled = stringReferencesEnabled;
    }

    public void setTemplatingEnabled(boolean templatingEnabled) {
        this.templatingEnabled = templatingEnabled;
    }

    public void setStringReferenceCleaningEnabled(
            boolean stringReferenceCleaningEnabled) {
    	throw new UnsupportedOperationException("StringReferenceCleaning is not currently implemented in OJSON.");
    }

	public void setMinimumStringLength(int minimumStringLength) {
		this.minimumStringLength = minimumStringLength;
	}

	public void setMinimumMatchingObjectKeys(int minimumMatchingObjectKeys) {
		this.minimumMatchingObjectKeys = minimumMatchingObjectKeys;
	}

	// Get the default String encoding for the JSON/OJSON payloads.
	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
}
