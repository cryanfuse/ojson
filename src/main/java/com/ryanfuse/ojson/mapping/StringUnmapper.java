package com.ryanfuse.ojson.mapping;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONString;

/*
 * Convert strings containing embedded OJSON tags into plain strings.
 */
public class StringUnmapper {
    private static final String STRING_REF = "@[0-9]+";
    private static final String STRING_REF_DECL = "@[0-9]+:";
    //private static final String TEMPLATE_NESTED = "@tn:";
    private static final String TEMPLATE_FLAT = "@t:";
    private static final String NO_VALUE_TAG = "@n";
    public static final String ESCAPE_TAG = "@e:";
    
    private Pattern stringRef = Pattern.compile(STRING_REF);
    private Pattern stringRefDecl = Pattern.compile(STRING_REF_DECL);
    
    private String string;
    private String stringPart;
    private String refPart;
    
    public StringUnmapper(String string) {
        this.string = string;
        Matcher stringRefDeclMatch = stringRefDecl.matcher(string);
        if (stringRefDeclMatch.find() && stringRefDeclMatch.start() == 0) {
            refPart = string.substring(0, stringRefDeclMatch.end() - 1);
            stringPart = string.substring(stringRefDeclMatch.end());
        } else {
            Matcher stringRefMatch = stringRef.matcher(string);
            // exact match "@<number>"
            if (stringRefMatch.matches()) {
                refPart = string;
            }
        }
    }

    public static boolean isFlatTemplate(String tag) {
        return tag.startsWith(TEMPLATE_FLAT);
    }

	public static String flatTemplateValue(String tag) {
		return tag.substring(TEMPLATE_FLAT.length());
	}

    /*
    public static boolean isNestedTemplate(String tag) {
        return tag.startsWith(TEMPLATE_NESTED);
    }
	*/

	/*
	 * Check for JSONStrings which contain reference tags
	 */
	public static Object unmapSimple(Object child, Map<String, String> stringRef) {
		if (!(child instanceof String)) {
			return child;
		}
		return StringUnmapper.unmapString((String)child, stringRef);
	}

    /*
     * Does this string begin with a reference tag? 
     */
	public static boolean isReference(String string) {
		StringUnmapper estring = new StringUnmapper(string);
		return estring.isStringRefDecl() || estring.isStringRef(); 
	}

    /*
     * Convenience method to use the global string reference map to process string
     * references and string reference declarations and return the original unmapped
     * plain string.
     */
    public static Object unmapString(String string, Map<String, String> stringRefMap) {
    	if (isEscapeTag(string)) {
    		return unescapeString(string);
    	}
    	StringUnmapper estring = new StringUnmapper(string);
    	if (estring.isStringRefDecl()) {
            stringRefMap.put(estring.refPart, estring.stringPart);
            return estring.stringPart;
        }
        if (estring.isStringRef()) {
            String stringRef = stringRefMap.get(estring.refPart);
            if (stringRef == null) {
            	return new ForwardStringReference(estring.refPart, stringRefMap);
            }
            return stringRef;
        }
        return string;
    }

	public static boolean matchesOjsonTag(String string) {
    	StringUnmapper estring = new StringUnmapper(string);
    	return estring.isStringRefDecl()
    			|| estring.isStringRef()
    			|| StringUnmapper.isFlatTemplate(string)
    			|| StringUnmapper.isEscapeTag(string)
    			|| StringUnmapper.isNoValueTag(string);
    }
    
    public static boolean isNoValueTag(String string) {
		return string.equals(NO_VALUE_TAG);
	}

	public static boolean isEscapeTag(String string) {
		return string.startsWith(ESCAPE_TAG);
	}
    
	// Strip the escape tag '@e:'.
	public static Object unescapeString(String string) {
    	if (string.length() == ESCAPE_TAG.length()) {
    		return "";
    	}
		return string.substring(ESCAPE_TAG.length());
	}


	public static int flatTemplateOffset() {
        return TEMPLATE_FLAT.length();
    }

    /*
    public static int nestedTemplateOffset() {
        return TEMPLATE_NESTED.length();
    }
    */

    /*
     * "@10:string"
     */
    public boolean isStringRefDecl() {
        return refPart != null && stringPart != null;
    }

    /*
     * "@10"
     */
    public boolean isStringRef() {
        return refPart != null && stringPart == null;
    }

    /*
     * The "@10:" part within "@10:string"
     */
    public String refPart() {
        return refPart;
    }

    /*
     * The "string" part within "@10:string"
     */
    public String stringPart() {
        return stringPart;
    }

    @Override
    public String toString() {
        return "string '" + string + "' [refPart '" + this.refPart + "', stringPart '" + this.stringPart + "']";
    }
}
