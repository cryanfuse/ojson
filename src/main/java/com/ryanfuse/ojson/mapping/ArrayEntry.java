/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping;

import java.util.ArrayList;
import java.util.List;

/*
 * A pairing of keys and values for representing an array mapping of
 * a JSON object. 
 */
public class ArrayEntry {

	private List<String> keys = new ArrayList<String>();
	private List<Object> values = new ArrayList<Object>();

	public void addPair(String key, Object value) {
		keys.add(key);
		values.add(value);
	}
	public List<String> getKeys() {
		return keys;
	}
	public void setKeys(List<String> keys) {
		this.keys = keys;
	}
	public List<Object> getValues() {
		return values;
	}
	public void setValues(List<Object> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("{");
		for (int i = 0; i < keys.size(); i++) {
			sb.append("'").append(keys.get(i)).append("': '");
			sb.append(values.get(i).toString()).append("'");
			if (i != keys.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append("}");
		return sb.toString();
	}
	
}
