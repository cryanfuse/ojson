/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ryanfuse.ojson.OJsonOptions;
import com.ryanfuse.ojson.mapping.builder.JsonArrayBuilder;
import com.ryanfuse.ojson.mapping.builder.JsonObjectBuilder;

/*
 * Maps optimised JSON text back into plain JSON, converting templated
 * arrays back to objects and de-referencing strings.
 */
public class OJsonToJsonMapper {

	private OJsonOptions options;
	private JSONObject ojson;
	private JSONArray jsonArray;
	private Map<String, String> stringRef = new HashMap<String, String>();
	private List<JSONObject> forwardRef = new ArrayList<JSONObject>();

	public OJsonToJsonMapper(JSONObject ojson, OJsonOptions options) {
		this.ojson = ojson;
		this.options = options;
	}
	
	public OJsonToJsonMapper(JSONArray jsonArray, OJsonOptions options) {
		this.jsonArray = jsonArray;
		this.options = options;
	}

	public String mapToString() {
		if (ojson != null) {
			JsonObjectBuilder ob = new JsonObjectBuilder(stringRef, forwardRef, options);
			ob.putAnonymousObject(ojson);
			resolveForwardReferences();
			Object jobj = ob.build();
			return jobj.toString();
		}
		if (jsonArray != null) {
			JsonArrayBuilder ab = new JsonArrayBuilder(stringRef, forwardRef, options);
			ab.addAnonymousArray(jsonArray);
			resolveForwardReferences();
			JSONArray jarr = ab.build();
			return jarr.toString();
		}
		return "null";
	}

	/*
	 * Resolve all object keys containing forward string references (eg "@10")
	 */
	private void resolveForwardReferences() {
		if (forwardRef.isEmpty()) {
			return;
		}
		for (JSONObject obj : forwardRef) {
			String[] keys = JSONObject.getNames(obj);
			for (String key : keys) {
				if (StringUnmapper.isReference(key)) {
					String string = stringRef.get(key);
					//System.out.println("++++ obj key FwdRef from " + key + " to " + string);
					if (string == null) {
						// panic - unresolved reference
						throw new JSONException("String reference '" + key + "' for object key is not mapped to a string.");
					}
					// Update the resolved key
					Object value = obj.get(key);
					obj.remove(key);
					obj.put(string, value);
				}
			}
		}
	}
}
