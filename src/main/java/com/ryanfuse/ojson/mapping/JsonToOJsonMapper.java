/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ryanfuse.ojson.OJsonOptions;
import com.ryanfuse.ojson.mapping.builder.OJsonArrayBuilder;
import com.ryanfuse.ojson.mapping.builder.OJsonObjectBuilder;

/*
 * Entry point for accepting JSON text and mapping to OJSON
 */
public class JsonToOJsonMapper {

	private OJsonOptions options;
	private JSONObject json;
	private JSONArray jsonArray;
	private StringMapper stringMapper;
	
	public JsonToOJsonMapper(JSONObject json, OJsonOptions options) {
		this.json = json;
		this.options = options;
		this.stringMapper = new StringMapper(options);
	}

	public JsonToOJsonMapper(JSONArray jsonArray, OJsonOptions options) {
		this.jsonArray = jsonArray;
		this.options = options;
		this.stringMapper = new StringMapper(options);
	}

	public String mapToString() {
		if (json != null) {
			OJsonObjectBuilder ob = new OJsonObjectBuilder(stringMapper, options);
			ob.putAnonymousObject(json);
			Object jobj = ob.build();
			return jobj.toString();
		}
		if (jsonArray != null) {
			OJsonArrayBuilder ab = new OJsonArrayBuilder(stringMapper, options);
			ab.addAnonymousArray(jsonArray);
			JSONArray jarr = ab.build();
			return jarr.toString();
		}
		return "null";
	}
}
