/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONString;

/*
 * A forward reference for a string tag eg "@10". Need to delay resolving the reference until
 * processing of the entire JSON text is complete. By the time toJSONString() is called,
 * the stringReferenceMap is expected to contain the unresolved tag.
 */
public class ForwardStringReference implements JSONString {

	private String ref;
	private Map<String, String> stringReferenceMap;

	public ForwardStringReference(String ref, Map<String, String> stringReferenceMap) {
		this.ref = ref;
		this.stringReferenceMap = stringReferenceMap;
	}

	/*
	 * Only called once the entire JSON text has been processed and all string references
	 * have been added to the reference map.
	 * 
	 * @see org.json.JSONString#toJSONString()
	 */
	@Override
	public String toJSONString() {
		String string = stringReferenceMap.get(ref);
		//System.out.println("+++++++ in FwdRef from " + ref + " to " + string);
		if (string == null) {
			throw new JSONException("String reference '" + ref + "' for value is not mapped to a string.");
		}
		return JSONObject.quote(string);
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public Map<String, String> getStringReferenceMap() {
		return stringReferenceMap;
	}

	public void setStringReferenceMap(Map<String, String> stringReferenceMap) {
		this.stringReferenceMap = stringReferenceMap;
	}
}
