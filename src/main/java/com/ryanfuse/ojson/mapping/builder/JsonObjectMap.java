/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping.builder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;

import com.ryanfuse.ojson.OJsonOptions;

/*
 * A map for the set of objects with partially/fully overlapping keys
 */
public class JsonObjectMap {

	private String objectKey;
	private List<JSONObject> objects;
	private Set<String> objectKeys;
	private OJsonOptions options;
	
	public JsonObjectMap(String objectKey, OJsonOptions options) {
		this.objectKey = objectKey; // null is unnamed
		this.options = options;
		objects = new ArrayList<JSONObject>();
		objectKeys = new HashSet<String>();
	}

	/*
	 * Conditional add() operation. Objects cannot be empty or, if not the initial object to be added,
	 * have a key set which does not partially or fully overlap with existing keys of objects already added.
	 */
	public boolean add(JSONObject parent) {
		return add(parent, null);
	}
	
	public boolean add(JSONObject parent, String parentKey) {
		if (objects.isEmpty()) {
			objectKey = parentKey;
			objects.add(parent);
			return addObjectKeys(parent.keySet());
		}
		if (parentKey != null) {
			if (objectKey == null || !objectKey.equals(parentKey)) {
				return false;
			}
		}
		if (!addObjectKeys(parent.keySet())) {
			return false;
		}
		objects.add(parent);
		return true;
	}

	private boolean addObjectKeys(Set<String> keySet) {
		if (keySet == null || keySet.isEmpty()) {
			return false;
		}
		if (objectKeys.isEmpty()) {
			objectKeys.addAll(keySet);
			return true;
		}
		Set<String> tmp = new HashSet<String>(objectKeys);
		tmp.retainAll(keySet);
		if (tmp.isEmpty()) {
			return false;
		}
		int min = options.getMinimumMatchingObjectKeys();
		// TODO: could apply this min check only when the new keyset is >= min  
		//if (keySet.size() >= min && tmp.size() < min)
		if (tmp.size() < min) {
			return false;
		}
		objectKeys.addAll(keySet);
		return true;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public List<JSONObject> getObjects() {
		return objects;
	}

	public void setObjects(List<JSONObject> objects) {
		this.objects = objects;
	}

	public Set<String> getObjectKeys() {
		return objectKeys;
	}

	public void setObjectKeys(Set<String> objectKeys) {
		this.objectKeys = objectKeys;
	}
}
