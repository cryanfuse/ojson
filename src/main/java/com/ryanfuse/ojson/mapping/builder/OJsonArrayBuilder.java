/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping.builder;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ryanfuse.ojson.OJsonOptions;
import com.ryanfuse.ojson.mapping.MappingUtils;
import com.ryanfuse.ojson.mapping.StringMapper;
import com.ryanfuse.ojson.mapping.UnmappableException;

/*
 * Build an Optimised JSON Array
 */
public class OJsonArrayBuilder {

	private enum MODE { NONE, OBJECT, ARRAY, SIMPLE };
	private MODE mode = MODE.NONE;
	
	private StringMapper stringMapper;
	private OJsonOptions options;
	private JSONArray target = new JSONArray();
	
	public OJsonArrayBuilder(StringMapper stringMapper, OJsonOptions options) {
		this.stringMapper = stringMapper;
		this.options = options;
	}

	// CALLED ONCE 
	public void addAnonymousArray(JSONArray parentArr) {
		if (!options.isTemplatingEnabled()) {
			simpleMapping(parentArr);
			return;
		}
		OJsonObjectBuilder objectBuilder = null;
		try {
			if (parentArr.length() < options.getMinimumArrayElements()) {
	    		throw new UnmappableException("Array with too few elements not mappable"); 
			}
			stringMapper.mark();
			for (int a = 0; a < parentArr.length(); a++) {
				Object element = parentArr.get(a);
			    switch (MappingUtils.jsonObjectType(element)) {
			    case OBJECT:
			    	if (!assertMode(MODE.OBJECT)) {
			    		throw new UnmappableException("Unexpected object element, array of mixed types not mappable"); 
			    	}
			        // array of objects - templating is possible if enough elements, with overlapping keys
			    	if (objectBuilder == null) {
			    		objectBuilder = new OJsonObjectBuilder(stringMapper, options);
			    	}
			    	objectBuilder.addAnonymousObject((JSONObject)element);
			        break;
			    case ARRAY:
			    	if (!assertMode(MODE.ARRAY)) {
			    		throw new UnmappableException("Unexpected array element, array of mixed types not mappable"); 
			    	}
			        // array of array.
			        for (int e = 0; e < parentArr.length(); e++) {
			            element = parentArr.get(e);
			        	OJsonArrayBuilder ab = new OJsonArrayBuilder(stringMapper, options);
						ab.addAnonymousArray((JSONArray) element);
						JSONArray elementArr = ab.build();
						target.put(elementArr);
			        }
			        break;
			    case SIMPLE:
			    	if (!assertMode(MODE.SIMPLE)) {
			    		return;
			    	}
			    	target.put(stringMapper.mapSimple(element));
			        break;
			    default:
			        break;
			    }
			}
			switch (mode) {
			case OBJECT:
				Object objOrArr = objectBuilder.build();
				// TODO should be an array in this case. 
				target = (JSONArray)objOrArr;
				break;
			case ARRAY:
				break;
			case SIMPLE:
				default:
			}
		} catch (UnmappableException e) {
			// Failed mapping - revert back to simple mapping
			stringMapper.reset();
			simpleMapping(parentArr);
		}
	}

	/*
	 *  Revert to simple mapping - string optimisations but no object to array mapping
	 */
	private void simpleMapping(JSONArray parentArr) {
		for (int a = 0; a < parentArr.length(); a++) {
			Object element = parentArr.get(a);
		    switch (MappingUtils.jsonObjectType(element)) {
		    case OBJECT:
				OJsonObjectBuilder objectBuilder = new OJsonObjectBuilder(stringMapper, options);
		    	objectBuilder.putAnonymousObject((JSONObject)element);
		    	Object elementObj = objectBuilder.build();
		    	target.put(elementObj);
		    	break;
		    case ARRAY:
		        // array of array.
		        for (int e = 0; e < parentArr.length(); e++) {
		            element = parentArr.get(e);
		        	OJsonArrayBuilder ab = new OJsonArrayBuilder(stringMapper, options);
					ab.addAnonymousArray((JSONArray) element);
					JSONArray elementArr = ab.build();
					target.put(elementArr);
		        }
		        break;
		    case SIMPLE:
		    	target.put(stringMapper.mapSimple(element));
		    	break;
		    default:
		        break;
		    }
		}
	}

	private boolean assertMode(MODE expectedMode) {
		if (mode == MODE.NONE) {
			mode = expectedMode;
			return true;
		}
		return (mode == expectedMode);
	}

	public JSONArray build() {
		return target;
	}
}
