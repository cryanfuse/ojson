/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONString;

import com.ryanfuse.ojson.OJsonOptions;
import com.ryanfuse.ojson.mapping.MappingUtils;
import com.ryanfuse.ojson.mapping.StringUnmapper;

/*
 * Build a plain JSON object from Optimised JSON. 
 */
public class JsonObjectBuilder {

	private enum MODE { NONE, ANON_SINGLE, NAMED_SINGLE, MAPPED_ARRAY };
	
	private MODE mode = MODE.NONE;
	
	private JSONObject targetObj = new JSONObject();
	private JSONArray targetArr = new JSONArray();
	private List<String> templateKeys = new ArrayList<String>();
	
	private Map<String, String> stringRef;
	private OJsonOptions options;
	private List<JSONObject> forwardRef;

	public JsonObjectBuilder(Map<String, String> stringRef, List<JSONObject> forwardRef, OJsonOptions options) {
		this.stringRef = stringRef;
		this.forwardRef = forwardRef;
		this.options = options;
	}

	/*
	 * Building a single object
	 */
	public void putAnonymousObject(JSONObject parent) {
		assertMode(MODE.ANON_SINGLE);
		mapChildrenToObject(parent, targetObj);
	}

	/*
	 * Building a single named object
	 */
	public void putNamedObject(String parentKey, JSONObject parent) {
		assertMode(MODE.NAMED_SINGLE);
		mapChildrenToObject(parent, targetObj);
	}

	public void addAnonymousArray(JSONArray element) {
		assertMode(MODE.MAPPED_ARRAY);
		if (templateKeys.isEmpty()) {
			// Key array. 1st array element, populate keys
			templateKeys.add(StringUnmapper.flatTemplateValue((String)element.get(0)));
			for (int i = 1; i < element.length(); i++) {
				String tag = (String)element.get(i);
				Object key = StringUnmapper.unmapString(tag, stringRef);
				if (key instanceof String) {
					templateKeys.add((String)key);
				} else {
					// forward reference
					templateKeys.add(tag); // original tag
				}
			}
			return;
		}
		// Value array - map to new object
		mapElementToObject(element);
	}

	/*
	 * Map an array of values to object values using the previously captured
	 * template keys.
	 */
	private void mapElementToObject(JSONArray element) {
		JSONObject obj = new JSONObject();
		for (int a = 0; a < element.length(); a++) {
			Object value = element.get(a);
			if (value == JSONObject.NULL) {
				continue;
			}
			String key = unmapObjectKey(templateKeys.get(a), obj);
		    switch (MappingUtils.jsonObjectType(value)) {
		    case OBJECT:
		    	JSONObject objectTarget = new JSONObject();
		    	mapChildrenToObject((JSONObject)value, objectTarget);
		    	obj.put(key, objectTarget);
		    	break;
		    case ARRAY:
            	JsonArrayBuilder childArrBuilder = new JsonArrayBuilder(stringRef, forwardRef, options);
    			childArrBuilder.addAnonymousArray((JSONArray) value);
    			JSONArray childArrObj = childArrBuilder.build();
    			obj.put(key, childArrObj);
		        break;
		    case SIMPLE:
		    	obj.put(key, StringUnmapper.unmapSimple(value, stringRef));
		    default:
		    	break;
		    }
		}
		targetArr.put(obj);
	}

	/*
	 * Map object to object, unmapping any string references
	 */
	private void mapChildrenToObject(JSONObject parent, JSONObject target) {
		Set<String> parentKeySet = parent.keySet();
		if (parentKeySet.isEmpty()) {
			// Empty object
	        target = parent;
			return;
		}
        for (String name : parentKeySet) {
        	String unmappedName = unmapObjectKey(name, target); 
            Object child = parent.get(name);
            switch (MappingUtils.jsonObjectType(child)) {
            case OBJECT:
            	JsonObjectBuilder childObjBuilder = new JsonObjectBuilder(stringRef, forwardRef, options);
            	childObjBuilder.putNamedObject(unmappedName, (JSONObject)child);
            	Object childObj = childObjBuilder.build();
            	// if collecting >1 object then store in target array (templating is occurring)
            	target.put(unmappedName, childObj);
                break;
            case ARRAY:
            	JsonArrayBuilder childArrBuilder = new JsonArrayBuilder(stringRef, forwardRef, options);
    			childArrBuilder.addAnonymousArray((JSONArray) child);
    			JSONArray childArrObj = childArrBuilder.build();
    			target.put(unmappedName, childArrObj);
                break;
            case SIMPLE:
    			target.put(unmappedName, StringUnmapper.unmapSimple(child, stringRef));
            default:
                break;
            }
        }
	}

	private String unmapObjectKey(String name, JSONObject target) {
		Object keyObj = StringUnmapper.unmapString(name, stringRef);
		if (keyObj instanceof String) {
			return (String)keyObj;
		}
		// mark this object as containing unresolved references
		forwardRef.add(target);
		return name; // original tag, to be resolved later
	}

	public Object build() {
		Object obj = null;
		if (mode == MODE.MAPPED_ARRAY) {
			obj = targetArr;
		} else {
			obj = targetObj;
		}
		return obj;
	}

	private void assertMode(MODE expectedMode) {
		if (mode == MODE.NONE) {
			mode = expectedMode;
			return;
		}
		if (mode != expectedMode) {
			//throw new UnmappableException("Invalid optimised JSON");
		}
	}
}
