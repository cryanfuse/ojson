/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping.builder;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ryanfuse.ojson.OJsonOptions;
import com.ryanfuse.ojson.mapping.MappingUtils;
import com.ryanfuse.ojson.mapping.StringUnmapper;

/*
 * Build a plain JSON array from Optimised JSON. 
 */
public class JsonArrayBuilder {

	private enum MODE { NONE, OBJECT, ARRAY, SIMPLE };
	private MODE mode = MODE.NONE;
	
	private Map<String, String> stringRef;
	private OJsonOptions options;
	private JSONArray target = new JSONArray();
	private List<JSONObject> forwardRef;
	
	public JsonArrayBuilder(Map<String, String> stringRef, List<JSONObject> forwardRef, OJsonOptions options) {
		this.stringRef = stringRef;
		this.forwardRef = forwardRef;
		this.options = options;
	}

	// Map array, possibly mapping elements back to objects. 
	public void addAnonymousArray(JSONArray parentArr) {
    	JsonObjectBuilder arrObjectBuilder = null;
    	boolean templatedObject = false;
		for (int a = 0; a < parentArr.length(); a++) {
			Object element = parentArr.get(a);
		    switch (MappingUtils.jsonObjectType(element)) {
		    case OBJECT:
		    	JsonObjectBuilder objectBuilder = new JsonObjectBuilder(stringRef, forwardRef, options);
		    	objectBuilder.putAnonymousObject((JSONObject)element);
		    	target.put(objectBuilder.build());
		     	break;
		    case ARRAY:
		    	// Array of arrays. Possibly unmap to object
		    	if (a == 0 && parentArr.length() > 1 && unmapTemplateEntry((JSONArray)element)) {
		    		templatedObject = true;
		    	}
		    	if (templatedObject) {
		    		// 1st child is a template entry
		    		// List<String> resultList = new ArrayList<String>();
		        	// resultList.add(StringUnmapper.flatTemplateValue(tag));
		    		if (arrObjectBuilder == null) {
		    			arrObjectBuilder = new JsonObjectBuilder(stringRef, forwardRef, options);
		    		}
			    	arrObjectBuilder.addAnonymousArray((JSONArray)element);
		    	} else {
		    		JsonArrayBuilder childArrBuilder = new JsonArrayBuilder(stringRef, forwardRef, options);
	    			childArrBuilder.addAnonymousArray((JSONArray) element);
	    			JSONArray childArrObj = childArrBuilder.build();
	    			target.put(childArrObj);
		    	}
		        break;
		    case SIMPLE:
		    	target.put(StringUnmapper.unmapSimple(element, stringRef));
		    default:
		    	break;
		    }
		}
		if (templatedObject) {
			target = (JSONArray)arrObjectBuilder.build();
		}
	}

	private boolean unmapTemplateEntry(JSONArray childElem) {
        // search for [ "@tn:name", ".." ]
        if (childElem.length() == 0) {
        	return false;
        }
        Object templ = childElem.get(0);
        if (!(templ instanceof String)) {
        	return false;
        }
        String tag = (String)templ;
        return StringUnmapper.isFlatTemplate(tag);
	}

	private boolean assertMode(MODE expectedMode) {
		if (mode == MODE.NONE) {
			mode = expectedMode;
			return true;
		}
		return (mode == expectedMode);
	}

	public JSONArray build() {
		return target;
	}
}
