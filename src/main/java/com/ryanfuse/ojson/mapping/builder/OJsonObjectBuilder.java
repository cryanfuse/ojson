/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ryanfuse.ojson.OJsonOptions;
import com.ryanfuse.ojson.mapping.ArrayEntry;
import com.ryanfuse.ojson.mapping.MappingUtils;
import com.ryanfuse.ojson.mapping.StringMapper;
import com.ryanfuse.ojson.mapping.UnmappableException;

/*
 * Build an Optimised JSON Object, or array of objects.
 */
public class OJsonObjectBuilder {

	private enum MODE { NONE, ANON_SINGLE, ANON_MULTI, NAMED_SINGLE, NAMED_MULTI };
	
	private MODE mode = MODE.NONE;
	
	private JsonObjectMap objectMap;
	private JSONObject targetObj = new JSONObject();

	private StringMapper stringMapper;
	private OJsonOptions options;

	private List<ArrayEntry> targetArr = new ArrayList<ArrayEntry>();
	
	public OJsonObjectBuilder(StringMapper stringMapper, OJsonOptions options) {
		this.stringMapper = stringMapper;
		this.options = options;
	}

	/*
	 * Building a single object
	 */
	public void putAnonymousObject(JSONObject parent) {
		assertMode(MODE.ANON_SINGLE);
		mapChildrenToObject(parent);
	}

	/*
	 * Building a single named object
	 */
	public void putNamedObject(String parentKey, JSONObject parent) {
		assertMode(MODE.NAMED_SINGLE);
		mapChildrenToObject(parent);
	}

	/*
	 * Building up multiple objects. Templating is possible. 
	 *  
	 * An object type, not the value for a key.
	 *   " {...} "
	 * IS CALLED MULTIPLE TIMES
	 */
	public void addAnonymousObject(JSONObject parent) throws UnmappableException {
		assertMode(MODE.ANON_MULTI);
		if (!getObjectMap(null).add(parent)) {
			// Entirely different key set, abandon attempt at fully optimised mapping
			throw new UnmappableException("objects with non-overlapping (or empty) keys not mappable to an array"); 
		}
		mapChildrenToArray(parent);
	}


	/*
	 * Add a child to the target object using the templated key name and possibly templated value.
	 */
	private void mapChildrenToObject(JSONObject parent) {
		Set<String> parentKeySet = parent.keySet();
		if (parentKeySet.isEmpty()) {
			// Empty object
	        targetObj = parent;
			return;
		}
        for (String name : parentKeySet) {
            String mappedName = stringMapper.mapString(name);
            Object child = parent.get(name);
            switch (MappingUtils.jsonObjectType(child)) {
            case OBJECT:
            	OJsonObjectBuilder childObjBuilder = new OJsonObjectBuilder(stringMapper, options);
            	childObjBuilder.putNamedObject(name, (JSONObject)child);
            	Object childObj = childObjBuilder.build();
            	// if collecting >1 object then store in target array (templating is occurring)
            	targetObj.put(mappedName, childObj);
                break;
            case ARRAY:
            	OJsonArrayBuilder childArrBuilder = new OJsonArrayBuilder(stringMapper, options);
    			childArrBuilder.addAnonymousArray((JSONArray) child);
    			JSONArray childArrObj = childArrBuilder.build();
    			targetObj.put(mappedName, childArrObj);
                break;
            case SIMPLE:
            	targetObj.put(mappedName, stringMapper.mapSimple(child));
                break;
            default:
                break;
            }
        }
	}

	private void mapChildrenToArray(JSONObject parent) {
		Set<String> parentKeySet = parent.keySet();
        ArrayEntry childArrEntry = new ArrayEntry();
		if (parentKeySet.isEmpty()) {
			// Empty object
	        targetArr.add(childArrEntry);
			return;
		}
        for (String name : parentKeySet) {
            Object child = parent.get(name);
            switch (MappingUtils.jsonObjectType(child)) {
            case OBJECT:
            	OJsonObjectBuilder objectBuilder = new OJsonObjectBuilder(stringMapper, options);
            	objectBuilder.putAnonymousObject((JSONObject)child);
            	Object childObj = objectBuilder.build();
            	// if collecting >1 object then store in target array (templating is occurring)
            	childArrEntry.addPair(name, childObj); // put an array (templated) or object
                break;
            case ARRAY:
            	OJsonArrayBuilder arrayBuilder = new OJsonArrayBuilder(stringMapper, options);
    			arrayBuilder.addAnonymousArray((JSONArray) child);
    			JSONArray childArrObj = arrayBuilder.build();
    			childArrEntry.addPair(name, childArrObj); // put an array
                break;
            case SIMPLE:
            	childArrEntry.addPair(name, stringMapper.mapSimple(child));
                break;
            default:
                break;
            }
        }
        targetArr.add(childArrEntry);
	}

	// Create an array of templated keys
	//   [ '@t:key1', 'key2', 'key3', ... ]
	private JSONArray buildTemplateKeys(Set<String> orderedKeys) {
		JSONArray keys = new JSONArray();
		orderedKeys.forEach(s -> {
			if (keys.length() == 0) {
				keys.put("@t:" + s);
			} else {
				keys.put(stringMapper.mapString(s));
			}
		});
		return keys;
	}

	/*
	 * Create an array of values for the next object.
	 * 	//   [ 'key1Val', 'key2Val', null, ... ]
	 * 
	 * Using the ordered set of all keys, iterate the child keys and generate an array
	 * row for values that match an ordered key. Fill in gaps with nulls. 
	 */
	private void buildChildKeys(List<String> childKeys, List<Object> childValues, JSONArray target, Set<String> allOrderedKeys) {
		JSONArray row = new JSONArray();
		for (String superKey : allOrderedKeys) {
			if (childKeys.contains(superKey)) {
				int i = childKeys.indexOf(superKey);
				Object childVal = childValues.get(i);
				row.put(childVal);
			} else {
				row.put(JSONObject.NULL);
			}
		}
		target.put(row);
	}

	/*
	 * Without templating a JSONObject is returned, otherwise an JSONArray of objects
	 */
	public Object build() {
		Object obj = null;
		switch (mode) {
		case ANON_SINGLE:
		case NAMED_SINGLE:
			obj = targetObj;
			break;
		case ANON_MULTI:
		case NAMED_MULTI:
		default:
			if (targetArr.size() == 0) {
				// empty object
				obj = targetObj;
			}
			JSONArray target = new JSONArray();
			// Build array templated key entry. This is a super set of all keys across all element objects
			Set<String> orderedKeys = objectMap.getObjectKeys();
			JSONArray keys = buildTemplateKeys(orderedKeys);
			target.put(keys);
			// Add generated array elements, each array is a templated object
			// for each targetArr entry, match up key sets with super-set
			for (ArrayEntry entry : targetArr) {
				List<String> childKeys = entry.getKeys();
				List<Object> childValues = entry.getValues();
				buildChildKeys(childKeys, childValues, target, orderedKeys);
			}
			obj = target;
			break;
		}
		return obj;
	}

	private void assertMode(MODE expectedMode) {
		if (mode == MODE.NONE) {
			mode = expectedMode;
			return;
		}
		if (mode != expectedMode) {
			// throw NotMappable
		}
	}

	/*
	 * Get map containing the super set of keys
	 */
	private JsonObjectMap getObjectMap(String objectKey) {
		if (objectMap == null) {
			objectMap = new JsonObjectMap(objectKey, options);
		}
		return objectMap;
	}
}
