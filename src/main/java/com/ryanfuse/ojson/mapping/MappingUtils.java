/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping;

import org.json.JSONArray;
import org.json.JSONObject;

public class MappingUtils {
    
	/*
	 * Determine the JSON type of an object
	 */
	public static ObjectType jsonObjectType(Object obj) {
        if (obj instanceof JSONObject) {
            return ObjectType.OBJECT;
        } else if (obj instanceof JSONArray) {
            return ObjectType.ARRAY;
        }
        return ObjectType.SIMPLE;
    }
}
