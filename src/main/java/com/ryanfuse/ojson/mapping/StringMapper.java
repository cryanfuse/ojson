/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONString;

import com.ryanfuse.ojson.OJsonOptions;

/*
 * Map raw strings into string references to reduce repetitive strings.
 */
public class StringMapper {

	private Map<String, String> stringRef = new HashMap<String, String>();
	private List<String> markedRef = new ArrayList<String>();
	private boolean marked = false;
	private OJsonOptions options;
    private int nextRef = 0;
	private JSONString simple; 

	public StringMapper(OJsonOptions options) {
		this.options = options;
	}
	
	/*
	 * Map a simple type (not an array or object)
	 */
	public Object mapSimple(Object child) {
		if (!(child instanceof String)) {
			return child;
		}
		return mapString((String)child);
	}
	   
	public boolean matchesOjsonTag(String string) {
		return StringUnmapper.matchesOjsonTag(string);
	}

	public String escapeString(String string) {
		return StringUnmapper.ESCAPE_TAG + string;
	}

	/*
	 * New strings come back as "@1:value", existing strings come back as "@1"
	 */
    public String mapString(String val) {
    	if (matchesOjsonTag(val)) {
    		// plain json string happens to begin with an ojson tag, need to escape it
    		// with an '@e:' tag which will be unconditionally removed when unmapped.
    		return escapeString(val);
    	}
        if (!options.isStringReferencesEnabled()) {
            return val;
        }
        if (val.length() < options.getMinimumStringLength()) {
            return val; // too short, don't map it
        }
        String ref = stringRef.get(val);
        if (ref != null) {
            return ref;
        }
        ref = nextRef();
        stringRef.put(val, ref);
        if (marked) {
        	markedRef.add(val);
        }
        return ref + ":" + val;
    }

    private String nextRef() {
        return "@" + String.valueOf(++nextRef);
    }
    
	@Override
	public String toString() {
		if (simple != null) {
			return simple.toJSONString();
		}
		return this.toString();
	}

	// Mark string references created after this point
	public void mark() {
		if (marked) {
			// nested - ignore
			return;
		}
		markedRef.clear();
		marked = true;
	}

	// Remove any string references created since the last mark()
	public void reset() {
		if (!marked) {
			// not marking - ignore
			return;
		}
		marked = false;
		if (markedRef.isEmpty()) {
			return;
		}
		markedRef.forEach(val -> {
			stringRef.remove(val);
		});
		markedRef.clear();
	}
}
