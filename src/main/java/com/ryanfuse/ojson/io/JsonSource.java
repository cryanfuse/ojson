/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class JsonSource {

    private String jsonString;
    private InputStream jsonStream;
    private File jsonFile;

    public JsonSource(String jsonString) {
        this.jsonString = jsonString;
        this.jsonStream = OJsonIOUtils.toInputStream(jsonString);
    }
    
    public JsonSource(InputStream jsonStream) {
        this.jsonStream = jsonStream;
    }

    public JsonSource(File jsonFile) throws FileNotFoundException {
        this.jsonFile = jsonFile;
        this.jsonStream = new FileInputStream(jsonFile);
    }
    
    public InputStream getStream() {
        return jsonStream;
    }

    public Reader getReader() {
        return new InputStreamReader(jsonStream);
    }
}
