
# JSON Generation

Scripts used to generate random JSON text. Refer to this site:

	http://www.json-generator.com/w

which accepts a template JSON string and uses this to generate JSON text which can then be copied and pasted into a new test JSON data file.

# Creating new tests

Copy and existing template from within this directory into a new xx-template.json file. Refer to random-template.json as an example.

Edit the embedded scripting rules with number of structures to generate.

Copy the script into http://www.json-generator.com/ and Generate

Copy to Clipboard link from the site.

In an editor open a new JSON data file under test/resources/ojson named <file>.json

Open an existing Junit test case, for example com.ryanfuse.ojson.ToOJsonBuilderTest and add a new test method. An example which tests conversion from the generated JSON into OJSON and back, including assert that final text matches the test data, will look similar to this:

	@Test
    public void testNewRandom() throws Exception {
        readJsonFromFile("/ojson/newRandom.json");
        String toOJson = OJson.toOJSON(json);
        // reporting of the calculated % improvement in generated JSON payload size is optional:
        reportStats("new random", trimObject(json), toOJson);

        String toJson = OJson.toJSON(toOJson);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
	}

Run the test case to verify it passes.
