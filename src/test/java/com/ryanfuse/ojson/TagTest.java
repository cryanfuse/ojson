/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

/*
 * Tests handling of plain JSON containing OJSON tags and required escaping in generated OJSON text.
 */
public class TagTest extends AbstractJsonTest {

    private String json;
    private String ojson;
    
    @Test
    public void testEscapeTagInJson() throws Exception {
    	json = "@e";
    	ojson = "@e";
        String toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson: " + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@e:";
    	ojson = "@e:@e:";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson: " + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@e:foo";
    	ojson = "@e:@e:foo";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson: " + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@@e:";
    	ojson = "@@e:";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson: " + toOJson);
        assertEquals(ojson, toOJson);
    }

    @Test
    public void testStringRefTagInJson() throws Exception {
    	json = "@123";
    	ojson = "@e:@123";
        String toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@123xxx";
    	ojson = "@123xxx";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@44 foo";
    	ojson = "@44 foo";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);
    }

    @Test
    public void testStringRefDeclTagInJson() throws Exception {
    	json = "@123:";
    	ojson = "@e:@123:";
        String toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@123:foo";
    	ojson = "@e:@123:foo";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@44: foo";
    	ojson = "@e:@44: foo";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);
    }

    @Test
    public void testTemplateTagInJson() throws Exception {
    	json = "@t";
    	ojson = "@t";
        String toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@t:";
    	ojson = "@e:@t:";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@t:foo";
    	ojson = "@e:@t:foo";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@t : bah";
    	ojson = "@t : bah";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);
    }

    @Test
    public void testNoValueTagInJson() throws Exception {
    	json = "@n";
    	ojson = "@e:@n";
        String toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@n:";
    	ojson = "@n:";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@n x";
    	ojson = "@n x";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

    	json = "@n ";
    	ojson = "@n ";
        toOJson = OJson.toOJSON(json);
        System.out.println("from " + json + " generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);
    }

	@Test
    public void testMixedTagsNoStringReferences() throws Exception {
        json = readJsonFromFile("/ojson/tags.json");
        //String expectedOjson = readJsonFromFile("/ojson/tags-expected.json");
        String expectedOjson = readJsonFromFile("/ojson/tags-no-stringrefs-expected.json");

        OJsonOptions opts = OJsonOptions.defaultOptions();
        opts.setStringReferencesEnabled(false);
        opts.setMinimumMatchingObjectKeys(1);
        String toOJson = OJson.toOJSON(json, opts);
        System.out.println("generated tags ojson:\n" + toOJson);
        JSONAssert.assertEquals(toOJson, expectedOjson, JSONCompareMode.STRICT);
        
        String toJson = OJson.toJSON(toOJson);
        System.out.println("unmapped object json:\n" + toJson);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
	}

	@Test
    public void testMixedTagsWithStringReferences() throws Exception {
        json = readJsonFromFile("/ojson/tags.json");
        String expectedOjson = readJsonFromFile("/ojson/tags-stringrefs-expected.json");

        OJsonOptions opts = OJsonOptions.defaultOptions();
        opts.setStringReferencesEnabled(true);
        opts.setMinimumMatchingObjectKeys(1);
        opts.setMinimumStringLength(1); // allow all strings to be refs

        String toOJson = OJson.toOJSON(json, opts);
        System.out.println("generated tags ojson:\n" + toOJson);
        JSONAssert.assertEquals(toOJson, expectedOjson, JSONCompareMode.STRICT);
        
        String toJson = OJson.toJSON(toOJson);
        System.out.println("unmapped object json:\n" + toJson);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
	}
}
