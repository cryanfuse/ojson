/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class OJsonBuilderSimpleTest extends AbstractJsonTest {

    private String json;
    private String ojson;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSimple() throws Exception {
    	json = "null";
     	ojson = "null";
    	//System.out.println("original simple json:\n" + json);
        String toOJson = OJson.toOJSON(json);
        //System.out.println("generated simple ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

        json = "true";
     	ojson = "true";
    	//System.out.println("original simple json:\n" + json);
        toOJson = OJson.toOJSON(json);
        //System.out.println("generated simple ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);

        json = "1234";
     	ojson = "1234";
    	//System.out.println("original simple json:\n" + json);
        toOJson = OJson.toOJSON(json);
        //System.out.println("generated simple ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);
    }

    @Test
    public void testEmptyObject() throws Exception {
    	json = "{}";
     	ojson = "{}";
    	//System.out.println("original json:\n" + json);
        String toOJson = OJson.toOJSON(json);
        //System.out.println("generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);
    }
     
    @Test
    public void testEmptyArray() throws Exception {
        json = "[]";
     	ojson = "[]";
    	//System.out.println("original json:\n" + json);
        String toOJson = OJson.toOJSON(json);
        //System.out.println("generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);
    }
    
    @Test
    public void testEmptyArrayOfObject() throws Exception {
        json = "[{}]";
     	ojson = "[{}]";
    	//System.out.println("original json:\n" + json);
        String toOJson = OJson.toOJSON(json);
        //System.out.println("generated ojson:\n" + toOJson);
        assertEquals(ojson, toOJson);
    }
    
    @Test
    public void testSimpleString() throws Exception {
        json = readJsonFromFile("/ojson/simple-strings.json");
        testBiDirMapping(json);
    }
}
