/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;

import org.json.JSONObject;
import org.junit.Test;

public class PerformanceTest extends AbstractJsonTest {

    private String json;
    private long start, end;

    // A test using json.hpack test data found here:
    // https://github.com/WebReflection/json.hpack/tree/master/test
	@Test
    public void testHpack5000Records() throws Exception {
        json = readJsonFromFile("/ojson/hpack.json");
        String toOJson = OJson.toOJSON(json);
        reportStats("json.hpack-5000", json, toOJson);
	}

	@Test
    public void test1000CustomerRecords() throws Exception {
        json = readJsonFromFile("/ojson/perf-1000.json");
        String toOJson = OJson.toOJSON(json);
        //System.out.println("generated 1000 ojson:\n" + toOJson);
        String originalJson = new JSONObject(json).toString();
        System.out.println("original json:\n" + originalJson);
        //reportStats("perf-1000-records", originalJson, toOJson);
	}

	@Test
    public void test100CustomerRecords() throws Exception {
        json = readJsonFromFile("/ojson/perf-100.json");
        startTimer();
        String toOJson = OJson.toOJSON(json);
        endTimer();
        System.out.println("generated 100 ojson:\n" + toOJson);
        String originalJson = new JSONObject(json).toString();
        System.out.println("original json:\n" + originalJson);
        reportStats("perf-100-records", originalJson, toOJson);
	}

	@Test
    public void testCompareCompressToOJson() throws Exception {
		json = readJsonFromFile("/ojson/perf-1000.json");
        // OJson measure of speed and 
        startTimer();
        String toOJson = OJson.toOJSON(json);
        long ojdur = endTimer();
        String gain = percentageGain(json, toOJson);
        
        startTimer();
        String compressedJson = compress(json);
        long comdur = endTimer();
        String gainCompress = percentageGain(json, compressedJson);
        
        System.out.println("OJson    took [" + ojdur + "] msec with reduction " + gain + "%");
        System.out.println("Compress took [" + comdur + "] msec with reduction " + gainCompress + "%");
	}

	private long endTimer() {
		this.end = System.currentTimeMillis();
		long dur = (end-start);
		//System.out.println("Elapsed time [" + dur + "] msec");
		return dur;
	}

	private void startTimer() {
		this.start = System.currentTimeMillis();
	}

	public static String compress(String str) throws Exception {
		if (str == null || str.length() == 0) {
            return str;
        }
        //System.out.println("String length : " + str.length());
        ByteArrayOutputStream obj=new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(obj);
        gzip.write(str.getBytes("UTF-8"));
        gzip.close();
        String outStr = obj.toString("UTF-8");
        //System.out.println("Output String length : " + outStr.length());
        return outStr;
	}
}
