/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.InputStream;
import java.io.Reader;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

public class OJsonBuilderObjectTest extends AbstractJsonTest {

    private String json;
    private String ojson;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testObjectOfArray() throws Exception {
    	json = "{" +
      		   " 'karr': [" +
      		   "  { 'key1' : 'val1-0', 'key2': 'val2-0' }," +
      		   "  { 'key1' : 'val1-1', 'key2': 'val2-1' }," +
      		   "  { 'key1' : 'val1-2', 'key2': 'val2-2' }" +
      		   " ]" +
      		   "" +
      		   "}";
     	ojson = "{\"@1:karr\":[[\"@t:key1\",\"@8:key2\"],[\"@2:val1-0\",\"@3:val2-0\"],[\"@4:val1-1\",\"@5:val2-1\"],[\"@6:val1-2\",\"@7:val2-2\"]]}";
    	System.out.println("original object json:\n" + json);
    	OJsonOptions opts = OJsonOptions.defaultOptions();
    	opts.setMinimumArrayElements(2);
    	opts.setMinimumStringLength(2);
    	opts.setMinimumMatchingObjectKeys(2);
        String toOJson = OJson.toOJSON(json, opts);
        System.out.println("generated object ojson:\n" + toOJson);
        reportStats("objectOfArr", trimObject(json), toOJson);
        assertEquals(ojson, toOJson);

        String toJson = OJson.toJSON(toOJson, opts);
        System.out.println("unmapped object json:\n" + toJson);

        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
    }

	@Test
    public void testObjectRandomLong20() throws Exception {
		json = readJsonFromFile("/ojson/random-long-20.json");
        String toOJson = OJson.toOJSON(json);
        System.out.println("generated long random ojson:\n" + toOJson);
        reportStats("long-20 random", trimObject(json), toOJson);

        String toJson = OJson.toJSON(toOJson);
        System.out.println("unmapped long-200 random json:\n" + toJson);

        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
	}

	@Test
    public void testFileStreamReader() throws Exception {
		json = readJsonFromFile("/ojson/random-long-20.json");

		File f  = readJsonToFile("/ojson/random-long-20.json");
        String toOJson = OJson.toOJSON(f);
        String toJson = OJson.toJSON(toOJson);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
        // read the ojson from a file
		File of  = readJsonToFile("/ojson/random-long-20.ojson");
		toJson = OJson.toJSON(of);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
        
        Reader r = readJsonToReader("/ojson/random-long-20.json");
        toOJson = OJson.toOJSON(r);
        toJson = OJson.toJSON(toOJson);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);

        Reader or = readJsonToReader("/ojson/random-long-20.ojson");
        toJson = OJson.toJSON(or);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);

        InputStream is = readJsonToStream("/ojson/random-long-20.json");
        toOJson = OJson.toOJSON(is);
        toJson = OJson.toJSON(toOJson);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);

        InputStream ois = readJsonToStream("/ojson/random-long-20.ojson");
        toJson = OJson.toJSON(ois);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
	}

	@Test
    public void testObjectRandomLong200() throws Exception {
        json = readJsonFromFile("/ojson/random-long-200.json");
    	OJsonOptions opts = OJsonOptions.defaultOptions();
    	opts.setStringReferencesEnabled(false);

        String toOJson = OJson.toOJSON(json);
        //System.out.println("generated long-200 random ojson:\n" + toOJson);
        reportStats("long-200 random", trimObject(json), toOJson);

        String toJson = OJson.toJSON(toOJson);

        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
	}

	@Test
    public void testOverlappingKeysTooLow() throws Exception {
        json = readJsonFromFile("/ojson/object-keys.json"); // has 3 overlapping keys
    	OJsonOptions opts = OJsonOptions.defaultOptions();
    	opts.setMinimumMatchingObjectKeys(4);
    	opts.setStringReferencesEnabled(false);
    	String toOJson = OJson.toOJSON(json, opts);
        System.out.println("generated objkey:\n" + toOJson);
        // no change, only 3 keys overlap
        JSONAssert.assertEquals(json, toOJson, JSONCompareMode.STRICT);
	}
	
	@Test
    public void testOverlappingKeysJustEnough() throws Exception {
		json = readJsonFromFile("/ojson/object-keys.json"); 
		ojson = readJsonFromFile("/ojson/object-keys.ojson"); 
    	OJsonOptions opts = OJsonOptions.defaultOptions();
    	opts.setMinimumMatchingObjectKeys(3);
    	opts.setStringReferencesEnabled(false);
    	String toOJson = OJson.toOJSON(json, opts);
        System.out.println("generated objkey:\n" + toOJson);
        // mapped, just meets 3 key limit
        JSONAssert.assertEquals(ojson, toOJson, JSONCompareMode.STRICT);
	}
	
	@Test
    public void testOverlappingKeys() throws Exception {
		json = readJsonFromFile("/ojson/object-keys.json"); 
		ojson = readJsonFromFile("/ojson/object-keys.ojson"); 
    	OJsonOptions opts = OJsonOptions.defaultOptions();
    	opts.setMinimumMatchingObjectKeys(1);
    	opts.setStringReferencesEnabled(false);
    	String toOJson = OJson.toOJSON(json, opts);
        System.out.println("generated objkey:\n" + toOJson);
        // mapped, easily meets 1 key limit
        JSONAssert.assertEquals(ojson, toOJson, JSONCompareMode.STRICT);
	}
	
	@Test
    public void testObjectMixed() throws Exception {
        json = readJsonFromFile("/ojson/mixed.json");
        String toOJson = OJson.toOJSON(json);
        System.out.println("generated mixed ojson:\n" + toOJson);
        reportStats("mixed", trimObject(json), toOJson);

        String toJson = OJson.toJSON(toOJson);
        System.out.println("unmapped mixed json:\n" + toJson);

        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
	}
	
    @Test
    public void testObjectFlat() throws Exception {
        json = readJsonFromFile("/ojson/simple-obj-flat.json");
        testBiDirMapping(json);
    }

    @Test
    public void testComplexObjectNested() throws Exception {
        json = readJsonFromFile("/ojson/complex-obj-nested.json");
        testBiDirMapping(json);
    }

    @Test
    public void testComplexObjectFlat() throws Exception {
        json = readJsonFromFile("/ojson/complex-obj-flat.json");
        testBiDirMapping(json);
    }

    //@Test
    public void testComplexObjectFlatMismatchedKeys() throws Exception {
        json = readJsonFromFile("/ojson/complex-obj-flat-no-template.json");
        testBiDirMapping(json);
    }

    @Test
    public void testObjectNested() throws Exception {
        json = readJsonFromFile("/ojson/simple-obj-nested.json");
        testBiDirMapping(json);
    }

    @Test
    public void testNoTemplate() throws Exception {
        json = readJsonFromFile("/ojson/no-templating.json");
        testBiDirMapping(json);
    }

    @Test
    public void testShortRandom() throws Exception {
        json = readJsonFromFile("/ojson/random-short.json");
        testBiDirMapping(json);
    }

    @Test
    public void testLargeRandom() throws Exception {
        json = readJsonFromFile("/ojson/random.json");
        testBiDirMapping(json);
    }
}
