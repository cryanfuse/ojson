/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class OJsonBuilderThirdPartyTest extends AbstractJsonTest {

    private String json;
    private String ojson;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testPetstore() throws Exception {
        json = readJsonFromFile("/ojson/petstore.json");
        testBiDirMapping(json);
    }

    @Test
    public void testSwaggerShort() throws Exception {
        json = readJsonFromFile("/ojson/swagger-short.json");
        testBiDirMapping(json);
    }

    @Test
    public void testSwagger() throws Exception {
        json = readJsonFromFile("/ojson/swagger-schema.json");
        testBiDirMapping(json);
    }

	
}
