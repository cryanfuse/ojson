/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

public class OJsonBuilderForwardRefTest extends AbstractJsonTest {

    private String json;
    private String ojson;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

	@Test
    public void testForwardReferences() throws Exception {
    	ojson = "[" +
      		   "  {'@1:key':'@2:keyOrVal'}," +
      		   "  {'@1':'val2','@2':'val3','@3:val3':'@5'}," +
      		   "  {'@3:key3':'@5:val5'}" +
      		   "]";
    	json = "{\"a\":[{\"key\":\"keyOrVal\"},{\"keyOrVal\":\"val3\",\"val3\":\"val5\",\"key\":\"val2\"},{\"key3\":\"val5\"}]}";
    	String toJson = OJson.toJSON(trimArray(ojson));
        System.out.println("generated array json:\n" + toJson);
        // wrap in an object, this package can't parse arrays.
        JSONAssert.assertEquals(json, "{\"a\":" + toJson + "}", JSONCompareMode.STRICT);
    }
}
