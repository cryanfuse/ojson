/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import com.ryanfuse.ojson.io.OJsonIOUtils;

/*
 * Common test util methods
 */
public abstract class AbstractJsonTest {

	public String readJsonFromFile(String testFile) throws Exception {
        URL url = this.getClass().getResource(testFile);
        File jsonFile = new File(url.getFile());
        return OJsonIOUtils.readFileToString(jsonFile); 
    }

	public File readJsonToFile(String testFile) throws Exception {
        URL url = this.getClass().getResource(testFile);
        return new File(url.getFile());
    }

	public InputStream readJsonToStream(String testFile) throws Exception {
		return this.getClass().getResourceAsStream(testFile);
    }

	public Reader readJsonToReader(String testFile) throws Exception {
		InputStream is = this.readJsonToStream(testFile);
		return new InputStreamReader(is);
    }

    public String trimArray(String json) {
    	JSONArray arr = new JSONArray(json);
		return arr.toString();
	}

    public String trimObject(String json) {
    	JSONObject obj = new JSONObject(json);
		return obj.toString();
	}

    public int trimmed(String toOJson) {
        String[] lines = toOJson.split("\n");
        StringBuilder sb = new StringBuilder();
        for (String line : lines) {
            sb.append(line.trim());
        }
        //System.out.println("trimmed: " + sb.toString());
        return sb.length();
    }

    public void reportStats(String tag, String json, String ojson) {
        int jsonSz = trimmed(json);
        int ojsonSz = trimmed(ojson);
    	float diff = 100 - (100 * ojsonSz) / jsonSz;
    	System.out.printf(
    		"====> " + tag
    		+ ": Reduction: " + String.format("%.1f", diff) + "%%" 
            + " from [" + jsonSz + "] to optimised size [" + ojsonSz + "]\n"); 
    }
    
    public String percentageGain(String json, String ojson) {
        int jsonSz = trimmed(json);
        int ojsonSz = trimmed(ojson);
    	float diff = 100 - (100 * ojsonSz) / jsonSz;
    	return String.format("%.1f", diff);
    }
    
    public void testBiDirMapping(String json) {
        int tab = 2;
        JSONObject jsonObject = new JSONObject(json);
        String originalJson = jsonObject.toString(tab);
        System.out.println("org.json original json:\n" + originalJson);

        int jsonSz = trimmed(originalJson), ojsonSz = 0;
        String mappedToOJson = OJson.toOJSON(jsonObject);
        mappedToOJson = new JSONObject(mappedToOJson).toString(tab);
        ojsonSz = trimmed(mappedToOJson);
        System.out.println("org.json ojson:\n" + mappedToOJson);

        String mappedToJson = OJson.toJSON(mappedToOJson);
        mappedToJson = new JSONObject(mappedToJson).toString(tab);
        System.out.println("org.json mapped ojson back to json:\n" + mappedToJson);

        JSONAssert.assertEquals(originalJson, mappedToJson, JSONCompareMode.STRICT);
        int endJsonSz = trimmed(mappedToJson);
        
        float diff = 100 - (100 * ojsonSz) / jsonSz;
        System.out.printf(
           "Sizes [" + jsonSz + " reduced to " + ojsonSz + ", final json " + endJsonSz + "], improvement: " 
           + String.format("%.1f", diff) + "%%");
    }
}
