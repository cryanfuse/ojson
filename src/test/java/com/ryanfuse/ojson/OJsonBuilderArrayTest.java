/*
 * (C) Copyright 2016 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.ojson;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang.NotImplementedException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

public class OJsonBuilderArrayTest extends AbstractJsonTest {

    private String json;
    private String ojson;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //@Test
    public void testArrayTooFewElements() throws Exception {
    	throw new NotImplementedException();
    }

    @Test
    public void testArrayOfObjectsNoKeysCommonToAll() throws Exception {
    	json = "[" +
      		   "  { 'key1' : 'val1-0' }," +
      		   "  { 'key1' : 'val1-1', 'key2': 'val2-1' }," +
      		   "  { 'key1' : 'val1-2', 'key2': 'val2-2', 'key3' : 'val3-2' }," +
      		   "  { 'key2' : 'val2-3' }" +
      		   "]";
    	ojson = "[[\"@t:key1\",\"@8:key2\",\"@9:key3\"],[\"@1:val1-0\",null,null],[\"@2:val1-1\",\"@3:val2-1\",null],[\"@4:val1-2\",\"@5:val2-2\",\"@6:val3-2\"],[null,\"@7:val2-3\",null]]";
    	System.out.println("original array json:\n" + json);
    	OJsonOptions opts = OJsonOptions.defaultOptions();
    	opts.setMinimumArrayElements(2);
    	opts.setMinimumStringLength(2);
    	opts.setMinimumMatchingObjectKeys(1);
        String toOJson = OJson.toOJSON(json, opts);
        System.out.println("generated array ojson:\n" + toOJson);
        reportStats("arrayOfObj", trimArray(json), toOJson);
        assertEquals(ojson, toOJson);

        String toJson = OJson.toJSON(toOJson, opts);
        System.out.println("unmapped array json:\n" + toJson);

        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
    }

	@Test
    public void testArrayOfDifferentObjects() throws Exception {
    	json = "[" +
      		   "  {'key1':'val1'}," +
      		   "  {'key2':'val2','key3':'val3','key4':'val4'}," +
      		   "  {'key5':'val5'}" +
      		   "]";
    	// generated key orders may vary, JSONAssert handles this when comparing expected/actual
    	ojson = "[{\"@2:key1\":\"@3:val1\"},{\"@6:key3\":\"@7:val3\",\"@8:key4\":\"@9:val4\",\"@4:key2\":\"@5:val2\"},{\"@10:key5\":\"@11:val5\"}]";
    	System.out.println("original array json:\n" + json);
    	OJsonOptions opts = OJsonOptions.defaultOptions();
    	opts.setMinimumArrayElements(2);
    	opts.setMinimumStringLength(2);
    	String toOJson = OJson.toOJSON(trimArray(json), opts);
        System.out.println("generated array ojson:\n" + toOJson);
        reportStats("arrayOfDiffObj", trimArray(json), toOJson);
        //assertEquals(ojson, toOJson);
        JSONAssert.assertEquals(ojson, toOJson, JSONCompareMode.STRICT);
        
        String toJson = OJson.toJSON(toOJson, opts);
        System.out.println("unmapped object json:\n" + toJson);

        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
    }

    @Test
    public void testArrayObjectArray() {
    	json = "{ 'arr' : [ " 
				+ " { 'street' : '10 smith rd', 'suburb' : 'smithwick', 'postcode' : 3000, "
				+ "	  'addr_arr' : [ { 'keyStr' : 'string11', 'keyBool' : true }, { 'keyStr' : 'string12', 'keyBool' : true } "
				+ "   ] "
				+ " }, "
				+ " { 'street' : '12 jones rd', 'suburb' : 'jonesville', 'postcode' : 3001, "
				+ "	  'addr_arr' : [ { 'keyStr' : 'string21', 'keyBool' : true }, { 'keyStr' : 'string22', 'keyBool' : false } "
				+ "   ] "
				+ " }, "
				+ " { 'street' : '12 don st', 'suburb' : 'donvale', 'postcode' : 3002, "
				+ "	  'addr_arr' : [ { 'keyStr' : 'string31', 'keyBool' : false }, { 'keyStr' : 'string32', 'keyBool' : false } "
				+ "   ] "
				+ " } "
				+ "] }; ";
    	ojson = "{\"@1:arr\":[[\"@t:street\",\"@19:postcode\",\"@20:suburb\",\"@21:addr_arr\"],[\"@2:10 smith rd\",3000,\"@3:smithwick\",[{\"@6:keyStr\":\"@7:string11\",\"@5:keyBool\":true},{\"@5\":true,\"@6\":\"@8:string12\"}]],[\"@9:12 jones rd\",3001,\"@10:jonesville\",[{\"@5\":true,\"@6\":\"@12:string21\"},{\"@5\":false,\"@6\":\"@13:string22\"}]],[\"@14:12 don st\",3002,\"@15:donvale\",[{\"@5\":false,\"@6\":\"@17:string31\"},{\"@5\":false,\"@6\":\"@18:string32\"}]]]}";

    	System.out.println("original array json:\n" + json);
    	OJsonOptions opts = OJsonOptions.defaultOptions();
    	opts.setMinimumArrayElements(2);
    	opts.setMinimumStringLength(2);
        String toOJson = OJson.toOJSON(json, opts);
        
        System.out.println("generated array ojson:\n" + toOJson);
        reportStats("arrayObjArr", trimObject(json), toOJson);
        assertEquals(ojson, toOJson);

        String toJson = OJson.toJSON(toOJson, opts);
        System.out.println("unmapped object json:\n" + toJson);

        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
    }

    // Verify json.hpack json maps to ojson and back without error.
    @Test
    public void testArrayFromJsonHpack() throws Exception {
    	json = readJsonFromFile("/ojson/hpack.json");
        String toOJson = OJson.toOJSON(json);
        String toJson = OJson.toJSON(toOJson);
        JSONAssert.assertEquals(json, toJson, JSONCompareMode.STRICT);
    }
}
