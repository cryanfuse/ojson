OJSON Optimised JSON

(C) Copyright 2016 Craig Ryan. All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License
(LGPL) version 2.1 which accompanies this distribution, and is 
available at http://www.gnu.org/licenses/lgpl-2.1.html

CRGREP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

----------------------------------------
This product includes software developed by:
----------------------------------------
The Apache Software Foundation
http://www.apache.org/
Rely on LICENSE-APACHE2
Apache Commons
http://commons.apache.org/
----------------------------------------
JSON.org
http://www.json.org/
Rely on LICENSE-JSONORG
