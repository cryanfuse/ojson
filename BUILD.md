
# Build tasks

Requires gradle installation.

## Setup

Install the gradlew wrapper:

```gradle wrapper```

## Build a distributions


```gradle build```

### Application and Distribution plugins

https://docs.gradle.org/current/userguide/application_plugin.html 
https://docs.gradle.org/current/userguide/distribution_plugin.html

The distribution plugin builds the distro ZIP/TAR files. The application plugin depends on the distribution plugin by adding executable application scripts. 

Distribution tasks:

Build tar and zip distros into _build/distribution_

```gradle assembleDist```

Install the application into _build/install_.

```gradle installDist```

Application tasks:

Build a exploded distro into _build/install_

```gradle assemble```

```gradle ```
```gradle ```

