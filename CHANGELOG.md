# OJSON - Optimised JSON

A log of changes made in each release.

# CHANGE LOG

0.2 (18-Jul-2016)
-----------------

### New features

* Added -u option to the Java Client to map back from OJSON to JSON
* More performance test cases including a 'hpack' example.

### Bug fixes

* Fixed OJson.formattedText() to return array strings (was throwing an exception)
* New OJson.compressedText() utility method to remove whitespace from text, to allow accurate statistics reporting
* Fixed Java Client to correctly report percentage increase in mapping OJSON back to plain JSON

### Documentation updates

* Updated description for use of Client
* Changed copyright from a range to simply 2016
* Added JSON.org license for completeness

0.1 (16-Jul-2016)
-----------------

First public release.

### Features

* Initial implementation with Java API
* Simple Java Client included to map JSON to Optimised JSON from the command line


